# About
The Salt Lake Tribune posted an article about Utah Bill HB330, allegedly pushed by the LDS church to limit the use of private recordings in court. The article was then changed on Feb 13. This highlights what was replaced.

## [Diff comparision](https://gitlab.com/xmo/hb330-diff/commit/d58bca6c6d185c62f5e2f7430fe8955a878f15d7?view=parallel)

Click [here](https://gitlab.com/xmo/hb330-diff/commit/d58bca6c6d185c62f5e2f7430fe8955a878f15d7?view=parallel) to view a diff comparing the original version with the Feb 13 update.

## Sources

Feb 6 Original: [Mormon church backs bill that could prevent recording bishop interviews](https://web.archive.org/web/20180206213711/https://www.sltrib.com/news/2018/02/06/mormon-church-interested-in-bill-that-could-prevent-recording-bishop-interviews/) • 4998 chars

Feb 13: [Utahns spoke loudly against recording bill supported by the Salt Lake Chamber and Mormon church, so lawmakers have dropped it](https://web.archive.org/web/20180321130331/https://www.sltrib.com/news/2018/02/06/mormon-church-interested-in-bill-that-could-prevent-recording-bishop-interviews/) • 2389 chars


Original url: sltrib.com/news/2018/02/06/mormon-church-interested-in-bill-that-could-prevent-recording-bishop-interviews/
Current url:  sltrib.com/news/2018/02/06/mormon-church-interested-in-bill-that-could-prevent-recording-bishop-interviews/

The URL is the same, confirming that this is not a redirect or an addendum. The entire article was replaced.

